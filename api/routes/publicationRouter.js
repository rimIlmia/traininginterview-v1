//Imports
const express = require("express");
const cors = require("cors");
const publicationController = require("../controllers/publicationController");
const auth=require("../middelware/auth")
const path = require("path");
const publicationRouter = express.Router();
publicationRouter.use(cors({ credentials: true, origin: "http://localhost:4200" }));
exports.router = (function() { 
publicationRouter.route("/").post(publicationController.store);
publicationRouter.route("/user/:id/").get(publicationController.getVideosByUser);
publicationRouter.route("/:id").delete(publicationController.delete);
publicationRouter.route("/").get(publicationController.all);

  return publicationRouter;
})();