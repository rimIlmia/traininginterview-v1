//Imports
const express = require("express");
const bodyParser = require("body-parser"); 
const userRouter = require("./modules/user/userRouter").router;
const authRouter=require("./modules/auth/authRouter").router;
const publicationRouter=require("./modules/publication/publicationRouter").router;
const commentRouter=require("./modules/comment/commentRouter").router;
const cors = require("cors");
const dotenv = require("dotenv");
const expressValidator = require('express-validator')
dotenv.config();
//Instantiate server
const server = express();
//port
const port = process.env.PORT;
//body-parser configuration
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
// access to api
server.use(cors());
//Routes
server.use('/api/v1/',authRouter);
server.use('/api/v1/user',userRouter);
server.use('/api/v1/publication',publicationRouter);
server.use('/api/v1/comment',commentRouter);
//Launch server
server.listen(port, res => {
  console.log("Serveur fonctionne sur le port " + port);
});
