const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();
module.exports = {
  generateTokenForUser: function(userData) {

    return jwt.sign(
        { idUser: userData.idUser  },
      process.env.JWT_SIGN_SECRET,
      { expiresIn: "1h" }
    );
  },
  parseAuthorization: function(authorization) {
    return authorization != null ? authorization.replace("Bearer ", "") : null;
  },
  getUserId: function(authorization) {
    let userId = -1;
    let token = module.exports.parseAuthorization(authorization);
    if (token != null) {
      try {
        let jwtToken = jwt.verify(token, process.env.JWT_SIGN_SECRET);
          if (jwtToken != null) userId = jwtToken.idUser;
      } catch (err) {
        
      }
    }
    return userId;
  }
};
