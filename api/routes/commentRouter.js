//Imports
const express = require("express");
const cors = require("cors");
const commentController = require("../controllers/commentController");
const auth=require("../middelware/auth")
const path = require("path");
const commentRouter = express.Router();
commentRouter.use(cors({ credentials: true, origin: "http://localhost:4200" }));
exports.router = (function() { 
commentRouter.route("/").post(commentController.store);
commentRouter.route("/video/:id").get(commentController.getCommentsByVideo);
commentRouter.route("/:id").delete(commentController.delete);
  return commentRouter;
})();