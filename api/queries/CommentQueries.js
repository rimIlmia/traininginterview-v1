 const connexion = require("../config/database");
module.exports = {
    create:(comment)=>{
            let sql=
      "INSERT INTO commentaires (content, date, videos_idVideo, users_idUser) values(?,?,?,?)";
      return new Promise((resolve, reject) => {
      connexion.query(sql, [comment.content, new Date(), comment.videos_idVideo, comment.users_idUser] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
    },
deleteByVideo:(idVideo)=>{

    let sqlQuery= "DELETE FROM commentaires WHERE videos_idVideo = ?"  ;

     return new Promise((resolve, reject) => {
            connexion.query(sqlQuery,[idVideo],(err, rows) => {
             
                if (err) {
                    reject(err)}
                    else
                resolve(true)
            })
        })
},
getCommentsByVideo:(idVideo)=>{
  console.log("qe")
    let videos_idVideo = idVideo;
    let sql =
      "SELECT * FROM commentaires inner join users on users.idUser = commentaires.users_idUser WHERE commentaires.videos_idVideo=? ORDER BY date DESC";

    return new Promise((resolve, reject) => {
      connexion.query(sql,[videos_idVideo] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
},
delete:(id)=>{
     sql = "DELETE FROM commentaires WHERE idCommentaire = ?"
    return new Promise((resolve, reject) => {
      connexion.query(sql,[id] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
}
}
 