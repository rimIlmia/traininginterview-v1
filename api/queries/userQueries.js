const connexion = require("../config/database");
const bcrypt = require("bcryptjs");

module.exports =  {
  register: (user) => {
      sql ="INSERT INTO users ( lastName, firstName,password, email) values(?,?,?,?)";
        return new Promise((resolve, reject) => {
            connexion.query(sql,[user.firstName,user.lastName,user.password,user.email],(err, rows) => {
             
                if (err) {
                    console.log("err",err)
                    reject(err)}

                resolve(rows[0])
            })
        })

        },
  getUserByEmail: (email) => {
    
        let sqlQuery = "SELECT * FROM users WHERE email=?";

        return new Promise((resolve, reject) => {
            connexion.query(sqlQuery,[email],(err, rows) => {
             
                if (err) {
                    console.log("err",err)
                    reject(err)}

                resolve(rows[0])
            })
        })
    },
  getUserById:(id)=>{
            
        let sqlQuery = "SELECT * FROM users WHERE idUser=?";

        return new Promise((resolve, reject) => {
            connexion.query(sqlQuery,[id],(err, rows) => {
             
                if (err) {
                    reject(err)}

                resolve(rows[0])
            })
        })
    
},
update:(user)=>{

 let sql ="UPDATE users SET firstName=?,lastName=?, metier=?, pays=?, ville=?, linkedin=?, facebook=?, description=?, photoUrl=? WHERE idUser=?";

      return new Promise((resolve, reject) => {
            connexion.query(sql,[user.firstName,user.lastName,user.metier,user.pays, user.ville,user.linkedin,user.facebook,user.description,user.photoUrl,user.idUser],(err, rows) => {
                if (err) {
                    reject(err)}
                resolve(true)
            })
        })
  },
getUserByVideo:(idVideo)=>{
  let sql="SELECT users.idUser,users.firstName,users.lastName, users.pays,users.ville,users.facebook,users.linkedin,users.email,users.age,users.photoUrl FROM users INNER JOIN videos ON users.idUser= videos.idUser WHERE videos.idVideo=?"
       return new Promise((resolve, reject) => {
      connexion.query(sql, [idVideo] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
    
  }   
}

