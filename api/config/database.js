const mysql = require("mysql");
const dotenv = require("dotenv");
dotenv.config();
//connexion to bd
connexion = mysql.createConnection({
  database: process.env.DB_DATABASE,
  host: process.env.DB_HOST,
  user: process.env.DB_USERNAME,
  password: process.DB_PASSWORD,
});
connexion.connect(function (err) {
  console.log("Connexion BD ok!")
  if (err) throw err;
  return err;
});
module.exports = connexion;