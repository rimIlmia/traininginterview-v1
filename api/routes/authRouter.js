//Imports
const express = require("express");
const cors = require("cors");
const authController = require("../controllers/authController");
const validation=require("../middelware/validation")
const authRouter = express.Router();
authRouter.use(cors({ credentials: true, origin: "http://localhost:4200" }));
exports.router = (function() { 
authRouter.route("/register").post(validation.validate('register'),authController.register);
authRouter.route("/login").post(validation.validate('login'),authController.login);
  return authRouter;
})();

