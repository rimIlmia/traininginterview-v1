 const connexion = require("../config/database");
module.exports = {
all:(res)=>{
 let sql =
      "SELECT * FROM videos inner join users on users.idUser = videos.idUser ORDER BY date DESC";
   return new Promise((resolve, reject) => {
      connexion.query(sql, (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
},
create:(video)=>{
let sql =
      "INSERT INTO videos (titreVideo, nomVideo, extension, idUser,date) values(?,?,?,?,?)";
  
 return new Promise((resolve, reject) => {
      connexion.query(sql,
      [video.titreVideo, video.nomVideo, video.extension, video.idUser, new Date()] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });


    },
getVideosByUser:(idUser)=>{
 let sql =
      "SELECT * FROM videos inner join users on users.idUser = videos.idUser  WHERE videos.idUser=? ORDER BY date DESC";
   return new Promise((resolve, reject) => {
      connexion.query(sql,
      [idUser] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
    }
    ,
delete:(idVideo,res)=>{
         let sql= "DELETE FROM videos WHERE idVideo =?";
     return new Promise((resolve, reject) => {
      connexion.query(sql,
      [idVideo] , (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
    }
}
 