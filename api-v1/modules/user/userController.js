//Imports
const userService=  require("./userService")
const { body, validationResult } = require('express-validator');

//Routes
module.exports = {
  profil:function(req,res){
  userService.profil(req,res).then(result =>
      res.status(result.status).send(result.payload))
},
  getUserById: (req, res)=> {
    let id = req.param("id");
    if (id == null) {
      return res.json({ success: false, msg: "Champs obligatoires" });   
    }
    userService.info(id).then(result =>
      res.status(result.status).send(result.payload));
  },
  getPhoto: (req, res)=>{
    
    return res.sendFile(process.cwd()+ "/public/images/" + req.param('id'));
  },
  update: (req, res)=> {
     const  idUser  = req.param("id")
  if(req.param("id")==null)
  {
    

    return res.json({success:false,msg:"errors update user"})

  } 
   let firstName = req.body.firstName;
    if (firstName !== null)
      firstName =
        firstName.charAt(0).toUpperCase() +
        firstName.substring(1).toLowerCase();
            console.log('first', firstName)


    let lastName = req.body.lastName;
    if (lastName !== null)
      lastName =
        lastName.charAt(0).toUpperCase() + lastName.substring(1).toLowerCase();

    let metier = req.body.metier;
    if (metier !== null)
      metier =
        metier.charAt(0).toUpperCase() + metier.substring(1).toLowerCase();

    let pays = req.body.pays;
    let ville = req.body.ville;
    if (ville !== null)
      ville = ville.charAt(0).toUpperCase() + ville.substring(1).toLowerCase();

    let linkedin = req.body.linkedin;
    let facebook = req.body.facebook;
    let description = req.body.description;
    let newUrlPhoto=null;
     if (description !== null)
      description =
        description.charAt(0).toUpperCase() +
        description.substring(1).toLowerCase();

    console.log("id",idUser)
   
  
     let userData={
             idUser:idUser,
             firstName:firstName,
             lastName:lastName,
             metier:metier,
             pays:pays,
             ville:ville,
             linkedin:linkedin,
             facebook:facebook,
             description:description,
             photoUrl:newUrlPhoto
    } 
  userService.update(userData).then(result =>
      res.status(result.status).send(result.payload));
  },
  getUserByVideo:(req, res)=>{ 
     const id=req.param("id")
     console.log('id',id)
     userService.getUserByVideo(id).then(result =>
      res.status(result.status).send(result.payload));
  }
 
};
