const multer = require('multer');
const storage = multer.diskStorage({
  destination: (req, file, callback) => {
      const typeFile = file.mimetype.split('/')[0];
      console.log(" typeFile ",  typeFile )
      if(typeFile==='image')
    callback(null, 'public/images');
    else if(typeFile=='video')
    callback(null,'public/videos')
    else {callback(new multer.MulterError("FILE_TYPE_NOT_SUPPORTED"))
  }
  },

  filename: (req, file, callback) => {
    const name = file.originalname.split(' ').join('_');
    const extension=file.mimetype.split('/').pop();
    callback(null, name + Date.now() + '.' + extension);
  }
});

/* defined filter */
const fileFilter= (req, file, cb) => {
  const type=req.url.split('/').pop();
  const extension=file.mimetype.split('/').pop();
  let acceptedFile=type==="photo"?["png","jpeg","jpg"]:type==="video"?["mp4"]:null
  if (acceptedFile!==null && acceptedFile.includes(extension)
  ) {
    cb(null, true);
  } else {
    cb(new multer.MulterError("TYPE_FILE_NOT_SUPPORTED", file), false);
  }
};
// max size image =7MO and  max size video=25MO
const uploadImage = multer({storage: storage,fileFilter:fileFilter, limits:{fileSize:7*1024*1204}}).single('myFile');
const uploadVideo = multer({storage: storage,fileFilter:fileFilter, limits:{fileSize:26*1024*1024}}).single('myFile');

const multImage=async(req,res,next)=>{
 
     uploadImage(req, res, (err)=> {

      if (err instanceof multer.MulterError) {
          return res.status(400).json({
        message:err
      })
      }
      else if (err) {
       return res.status(400).json({
        message:err
      })
   
    }
     next()
  })
  }

  const multVideo=async(req,res,next)=>{
 
     uploadVideo(req, res, (err)=> {

      if (err instanceof multer.MulterError) {
          return res.status(400).json({
        message:err
      })
      }
      else if (err) {
       return res.status(400).json({
        message:err
      })
   
    }
     next()
  })
  }
module.exports  ={
  multImage,multVideo
}

 



  
  
 

