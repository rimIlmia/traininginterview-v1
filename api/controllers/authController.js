//Imports
const authService=  require("../services/authService")
// Constants
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX = /^(?=.*\d).{8,12}$/;
const { validationResult } = require('express-validator');
module.exports = {
 register: async(req, res,next)=> {
     try {
      const error = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

      if (!error.isEmpty()) {
        res.status(422).json({ success:false,msg: error.array()[0].msg});
        return;
      }
    let data=req.body
    console.log(data)
    let firstName = data.firstName;
    firstName =
      firstName.charAt(0).toUpperCase() + firstName.substring(1).toLowerCase();

    let lastName = data.lastName;
    lastName =
      lastName.charAt(0).toUpperCase() + lastName.substring(1).toLowerCase();

    let password = data.password;
    let email = data.email;

    authService.register(req.body).then(result =>
      res.status(result.status).send(result.payload))
    }catch(err) {
     return next(err)
   }
  },
   login: function(req, res) {
       //params

   try {
      const error = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

      if (!error.isEmpty()) {
        res.status(422).json({ success:false,msg: error.array()[0].msg});
        return;
      }
     authService.login(req.body).then(result =>
      res.status(result.status).send(result.payload))}catch(err) {
     return next(err)
      
  }},
  };
 


