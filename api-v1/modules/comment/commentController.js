//Imports
const commentService=  require("./commentService")
//Routes
module.exports = {
  store:function(req,res){
  commentService.store(req.body).then(result =>
      res.status(result.status).send(result.payload))
},
getCommentsByVideo: (req,res) =>{  
  idVideo=req.param("id");
  commentService.getCommentsByVideo(idVideo).then(result =>
      res.status(result.status).send(result.payload))
},
delete:(req,res)=>{
  idComment=req.param("id");
  commentService.delete(idComment).then(result =>
      res.status(result.status).send(result.payload))
},
};
