//Imports
const authService=  require("../services/authService")
// Constants
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX = /^(?=.*\d).{8,12}$/;
const { body } = require('express-validator/check')
const { validationResult } = require('express-validator/check');

module.exports = {
validate: (method)=>{
  switch (method) {
    case 'register': {
      console.log("ici vali")
     return [ 
        body('firstName', 'firstname obligatoire').exists(),
        body('email', 'Invalid email').exists().isEmail(),
       ]   
    }
  }

},
  register: async(req, res,next)=> {
     try {
      const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

      if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
      }
    let data=req.body
    console.log(data)
    let firstName = data.firstName;
    firstName =
      firstName.charAt(0).toUpperCase() + firstName.substring(1).toLowerCase();

    let lastName = data.lastName;
    lastName =
      lastName.charAt(0).toUpperCase() + lastName.substring(1).toLowerCase();

    let password = data.password;
    let email = data.email;

    // if (
    //   email == null ||password == null ||firstName == null ||lastName == null) 
    //   {
    //   return res.status(400).json({ success: false, msg: "Champs obligatoires" });}
    // if (firstName.length >= 13 || firstName.length <= 2) {
    //   return res.status(400).json({
    //     success: false,
    //     msg: "First Name doit contenir entre 3-15 caractères"
    //   });
    // }
    // if (lastName.length >= 13 || lastName.length <= 2) {
    //   return res.status(400).json({
    //     success: false,
    //     msg: "LastName doit contenir entre 2-15 caractères"
    //   });
    // }
    // if (!EMAIL_REGEX.test(email)) {
    //   return res.status(400).json({ success: false, msg: "email is not valid" });
    // }
    // if (!PASSWORD_REGEX.test(password)) {
   
    //   return res.status(400).json({
    //     success: false,
    //     msg: "password invalid (must length 8-12 and include 1 number at least)"
    //   });
    // }
    authService.register(req.body).then(result =>
      res.status(result.status).send(result.payload))
    }catch(err) {
     return next(err)
   }
  },
   login: function(req, res) {
       //params
    let email = req.body.email;
    let password = req.body.password;

    console.log("email " + email + "  password   " + password);

    if (email == null || password == null) {
      console.log("Champs obligatoires");
      return res.json({ success: false, msg: "Champs obligatoires" });
      // res.json
    }
    if (!EMAIL_REGEX.test(email)) {
      console.log("email is not valid");
      return res.json({ success: false, msg: "email not valid" });
    }
     authService.login(req.body).then(result =>
      res.status(result.status).send(result.payload))
  },
  };
 


