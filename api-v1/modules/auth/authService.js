//Imports
const jwt = require("jsonwebtoken");
const jwtUtils = require("../../utils/jwt.utils");
const connexion = require("../../config/database");
const userQueries=require("../user/userQueries")
const bcrypt = require("bcryptjs");

//Routes
module.exports = {
  /**************************inscription***************************** */
  register:async (data)=> {
    //params
        const user = await userQueries.getUserByEmail(data.email);
        if(!user)
        {
          return bcrypt.genSalt().then((salt) => bcrypt.hash(data.password, salt)).then((hashedPassword) => {
            return userQueries.register({ 
            firstName: data.firstName,
            lastName:data.lastName,
            email:data.email,
            password:hashedPassword,
          })
          .then((response) => ({
            status: 201,
            payload: {
              success: true,
              msg: "Enregistré avec success",
            },
          }))
          .catch((err) => ({
            status: 400,
            payload: {
              success: false,
              msg: err,
            },
          }));
      });

    }
        else
        {
             return {
               status:400,
               payload: {
                            success: false,
                            msg: "Un compte a déjà été crée avec l’adresse " + user.email
                        }}
        } 
  },
  /***************************Login************************************ */
  login: async (data )=> {
  
  const user = await userQueries.getUserByEmail(data.email);
       
        if(!user)
        {
             return {
               status:400,
               payload:{success: false,
                msg: "L'e-mail entré ne correspond à aucun compte!"}
            
          }
        }
        else
        {
          const passwordMatched = await bcrypt.compare(data.password, user.password);
          if(passwordMatched)
          {
            return ({
              status:200,
               payload: {
                success: true,
                idUser: user.idUser,
                token: jwtUtils.generateTokenForUser(user),
                msg: "identifiants valides"
                        }
            })
          }
          else
          {
            return ({ status:400,
                payload:{
                   success: false,
                msg: "Le mot de passe que vous avez rentré n'est pas correct!"
                }

            })
          }
        }




             
  },

};
