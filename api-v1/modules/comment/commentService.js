//Imports
const commentQueries=require("./CommentQueries")
//Routes
module.exports = {
  store: async  (data) =>{
    let comment={
      content:data.comment,
      users_idUser : data.idUser,
      videos_idVideo : data.idVideo
	}
	    return commentQueries.create(comment).then(response => ({
				status: 200,
				payload: { success: true, msg:"Enregistré avec succes "}
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			}));
  },
  getCommentsByVideo:async(id)=>{
      return commentQueries.getCommentsByVideo(id).then(response => ({
				status: 200,
				payload: { success: true, data: response }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			}));
  },
  delete:async(id)=>{
    return commentQueries.delete(id).then(response => ({
				status: 200,
				payload: { success: true, msg:"supprimé avec success" }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			})); 
  },
 
};
