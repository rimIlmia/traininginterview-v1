-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mer. 04 nov. 2020 à 21:28
-- Version du serveur :  5.7.24
-- Version de PHP : 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `traininginterviewdb`
--

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

CREATE TABLE `commentaires` (
  `idCommentaire` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `videos_idVideo` int(11) NOT NULL,
  `users_idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commentaires`
--

INSERT INTO `commentaires` (`idCommentaire`, `content`, `date`, `videos_idVideo`, `users_idUser`) VALUES
(40, 'c\'est ma premiere video', '2020-09-24 14:09:23', 88, 46);

-- --------------------------------------------------------

--
-- Structure de la table `likes`
--

CREATE TABLE `likes` (
  `users_idUser` int(11) NOT NULL,
  `videos_idVideo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `idMessage` int(11) NOT NULL,
  `idUserFrom` int(11) NOT NULL,
  `idUserTo` int(11) NOT NULL,
  `content` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `idUser` int(11) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `metier` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `pays` varchar(255) DEFAULT NULL,
  `ville` varchar(255) DEFAULT NULL,
  `photoUrl` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`idUser`, `firstName`, `lastName`, `email`, `password`, `age`, `metier`, `description`, `facebook`, `linkedin`, `pays`, `ville`, `photoUrl`) VALUES
(36, 'Anne', 'Leroux', 'anne.leroux@gmail.com', '$2a$05$H0nSnbBrTsfxXVI3nw95I.j.x3a870beArJ1wS.Aw6XUcOgfPBu5q', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'default-profile-pic.jpg'),
(37, 'Julien', 'Ferlay', 'julien@yahoo.fr', '$2a$05$5sXeesW2hqBQARp4rC1bdeKK2yj9slvgNuZzKy9ZGTxx82XHPSfFO', NULL, 'Coach de vie ', 'Le coaching de vie permet de faire face à une situation qui paraît insurmontable pendant une période de changement ou de crise.', 'null', 'null', 'France', 'Paris', 'photo1579042378943.jpeg'),
(38, 'Dupont', 'Elise', 'elisa@gmail.com', '$2a$05$AT2fMLAbGvggFLt0hsi.9uxkz54YmwdIevnFp7jKdb0F4Pu9LD.q2', NULL, 'null', 'A la recherche de stage fin d’études ', 'null', 'null', 'France', 'Montreuil', 'photo1579043115574.jpeg'),
(39, 'Naguine', 'Lechat', 'naguine@gmail.com', '$2a$05$f9DWM9Vs0UxtDaQEBmKsJOIa./fSG6d/bnT6nU/nQghMpoRqNy.Zq', NULL, 'Blogueuse', 'A la recherche dun contrat dalternance', 'null', 'null', 'France', 'Lyon', 'photo1579044717822.png'),
(40, 'Pierre', 'Chaxel', 'pierre@yahoo.fr', '$2a$05$7CVC7gcE850wYpBsWpi/BezQhYweCkMJPTEcZLDYcGd2n1ft/2h/O', NULL, 'Musicien', 'Reconversion pro ', 'null', 'null', 'France', 'Paris', 'photo1579045921830.png'),
(41, 'Rim', 'Chakroun', 'Rim.chakroun.bizerte@gmail.com', '$2a$05$9EpoF1C.l4CMcV7NLduc8OPMznYmUXxOjC5I5ma6.drYWq4z8CTg6', NULL, 'null', 'Jjjj', 'null', 'null', 'Afrique_Centrale', 'null', 'default-profile-pic.jpg'),
(43, 'Louise', 'Franc', 'louise@gmail.com', '$2a$05$s8EKqOwlD4wuM0NXotdoFeLlG3hfYl8/9NeuvL.jEUbIR7dWGz2xu', NULL, 'Gestionnaire', 'En recherche poste vendeuse', 'null', 'null', 'France', 'Paris', 'photo1579078942948.jpeg'),
(44, 'Rim', 'Chakroun', 'sonia@gmail.com', '$2a$05$8bKvvelJmIXuCjLSievbT.QRfqGgCMo/kMvNE5EN6rPmR8xxCPlpy', NULL, 'null', 'Bonjour', 'null', 'null', 'Afghanistan', 'Montreuil', 'photo1579095444192.jpeg'),
(45, 'Julien', 'Julien', 'julien@gmail.com', '$2a$05$pxVDICfwHrnNqzH9MWSRB.OULoCGtk9xd/wZgMdEE4/.rrXAOaLX2', NULL, 'Devloppeur', 'Dev junior', 'null', 'null', 'France', 'Paris', 'photo1580303333458.png'),
(46, 'Rim', 'Chakroun', 'Rim.chakroun.pro@gmail.com', '$2a$05$Q12XiD4.5yTCjV5BAcaot.yKReawDSJhkP4pHWaiCvyZe.zrbYzF6', NULL, 'Developpeuse', 'Actuellement en formation', 'null', 'null', 'France', 'Paris', 'photo1600949049686.png'),
(47, 'Shelley', 'Shelley', 'cat@gmail.com', '$2a$05$HNtzJA4ghpYZ628T2BSucOelU1IJo4NdW1foz6fFyZNrSM0.WqITi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bonjour'),
(48, 'shelley', 'shelley', 'ct@gmail.com', '$2a$05$Wlu1J6C1cZdraIXwZnNWDOc9XeYirn1tDAkjh84NolIBEeDNEOYxu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bonjour'),
(49, 'shelley', 'shelley', 'bt@gmail.com', '$2a$05$6Bbe90/kt.dvRG/EITHB1un5Y7HrWxgWIdJg.IeMVpge9XBfSVnsC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bonjour'),
(50, 'Shelley', 'Shelley', 'amira@gmail.com', '$2a$05$oJ/g/cJ6CNAteF02efWAle6KWrs9.0T/.sKsrXHVY0pA3YMRzkWOG', NULL, 'null', 'null', 'null', 'null', 'null', 'null', 'photo1603548882708.png'),
(51, 'Shelley', 'Shelley', 'bonjour@gmail.com', '$2a$05$iqxArKrw0R.eABGfnKRxIOODllcTaECVt2q3nyYX5bnW6Rx09MdhS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'Nadine', 'Ilmia', 'nadine@gmail.com', '$2a$05$0G778ZLl/caGHHz9Pz6LAuyCEzkt0a77vGjJ4twdjrGxGUH2HUn4C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 'Biiii', 'Booo', 'tanina@gmail.com', '$2a$05$dbFQPx0VZqBX1zitsBeYZ.LT88RN.69A2aedHdakBus5g7AvFarxK', NULL, 'null', 'null', 'null', 'null', 'Algerie', 'null', 'photo1603568502755.jpeg'),
(54, 'Bonn', 'Boon', 'bonbon@gmail.com', '$2a$05$6GFDnhuZ78Q4AmvBrjNdfucKttubGGR/jOjWLmpWviYMKsbuRq3T2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'Test2', 'Test1', 'test123@gmail.com', '$2a$05$DU4WdJtLsvlIYB3RAzikD.K.a9ZST9WPoedjnMbRT2lwNI19I1yKm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'Quentin', 'Quentin', 'quentin@gmail.com', '$2a$05$zEnQ16YJAASv6ZlPR947dukQU3TkxUqqjWW4zy.ZIlor8U8A2ZaU6', NULL, 'Formateur', 'null', 'null', 'null', 'Anguilla', 'null', 'photo1603976741481.png'),
(57, 'binta', 'binta', 'binta@gmail.com', '$2a$05$kSKxbj/BnO22HAaac.FbRuD3fFCGPp50JnynzO1nuNFwFjT6sXrQy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'binta'),
(58, 'Yasmine1@gmail.com', 'Azerty1234', 'yasmine@gmail.com', '$2a$05$37llSz/e6ik5GLOm3Sr0POE6PAVEVLXJkZRMJZDiI950nNLumx7OO', NULL, 'Bon', '', '', 'lij', '', 'Ton', NULL),
(59, 'binta', 'yas', 'yasmin2e@gmail.com', '$2a$05$pbTMyv3hKbaibErA1uswi.vtVIwkM.LMBM9csWF.9aPxKkuHkBPT.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'binta'),
(60, 'yab', 'yab', 'ya@gmail.com', '$2a$10$jZP5MPWTv8isumcpIVYSCuGByvcefTYBaeRgmvOQbAfKxm66u7L7i', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'yab', 'yab', 'yarabi@gmail.com', '$2a$10$6pkJT6jcBBnYxCvquJ5CieCTqVc3vLzDzO2TPtgRl7QimCrrqlo7G', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'yab', 'yab', 'yarabigmail.com', '$2a$10$GwBIsqfNi4lxgDljNUgs4eilZZ5CB0IV1W1fkM7LZSJWC2EKN8WBG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'yab', '', 'yarai@gmail.com', '$2a$10$1QXCJRdf3hVbHYxYqgXPVeyZk9ueWIZo9ttDV1bPF6RFQWCmXqWMe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'yab', '', 'yaraibb@gmail.com', '$2a$10$ETo2HAFS4smoYRxjZZr5i.VyeHR0Kj/lkDpZCQVqn3v//dLMlNTXy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'yab', 'hbb', 'yara@gmail.com', '$2a$10$Aa/.a/J9v/mdRDfHxGDfT.x7t1pNlTyEj1phMBWd.CfOE4cIEZZIS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'yab', 'jega', 'yar@ggggg.com', '$2a$10$a0MEhaoaN8kaENFdi3ejweugKYm77nYyo.684rVJ65dXFssqlf3le', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `videos`
--

CREATE TABLE `videos` (
  `idVideo` int(11) NOT NULL,
  `titreVideo` varchar(255) NOT NULL,
  `nomVideo` varchar(255) NOT NULL,
  `extension` varchar(45) NOT NULL,
  `date` datetime NOT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `videos`
--

INSERT INTO `videos` (`idVideo`, `titreVideo`, `nomVideo`, `extension`, `date`, `idUser`) VALUES
(74, 'Parlez-moi de vous ?', 'video1579041903426', 'mp4', '2020-01-12 09:23:15', 36),
(75, 'Quels sont vos points forts ?', 'video1579041936904', 'mp4', '2020-01-12 10:45:37', 36),
(77, 'Parlez-moi de vous ?', 'video1579042203032', 'mp4', '2019-12-28 11:50:07', 37),
(78, 'Parlez-moi de vous ?', 'video1579044063000', 'mp4', '2019-12-19 14:21:05', 38),
(79, 'Que pouvez-vous nous apporter ?', 'video1579044091790', 'mp4', '2020-01-12 08:21:32', 38),
(80, 'Parlez-moi de vous ?', 'video1579044411663', 'mp4', '2019-12-20 16:00:00', 39),
(81, 'Parlez-moi de vous ?', 'video1579044983081', 'mp4', '2020-01-10 08:36:23', 39),
(88, 'Parlez-moi de vous ?', 'video1580303397971', 'mp4', '2020-01-29 14:10:00', 45),
(89, 'Parlez-moi de vous ?', 'video1600949247350', 'mp4', '2020-09-24 14:07:39', 46),
(90, 'Quels sont vos points forts ?', 'video1603550858018', 'mp4', '2020-10-24 16:47:39', 50),
(92, 'Parlez-moi de vous ?', 'video1603556336315', 'mp4', '2020-10-24 18:18:57', 53),
(93, 'Parlez-moi de vous ?', 'video1603976610758', 'mp4', '2020-10-29 14:03:31', 56),
(95, 'azerty123', 'shelley', 'shelley', '2020-11-01 18:09:21', 58),
(96, 'azerty123', 'shelley', 'txt', '2020-11-01 18:15:07', 58),
(97, 'azerty123', 'shelley', 'shelley', '2020-11-03 23:03:41', 58);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `commentaires`
--
ALTER TABLE `commentaires`
  ADD PRIMARY KEY (`idCommentaire`),
  ADD KEY `fk_commentaires_videos1_idx` (`videos_idVideo`),
  ADD KEY `fk_commentaires_users1_idx` (`users_idUser`);

--
-- Index pour la table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`users_idUser`,`videos_idVideo`),
  ADD KEY `fk_likes_users1_idx` (`users_idUser`),
  ADD KEY `fk_likes_videos1_idx` (`videos_idVideo`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`idMessage`),
  ADD KEY `fk_messages_users1_idx` (`idUserFrom`),
  ADD KEY `fk_messages_users2_idx` (`idUserTo`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUser`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- Index pour la table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`idVideo`),
  ADD KEY `fk_videos_users_idx` (`idUser`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `commentaires`
--
ALTER TABLE `commentaires`
  MODIFY `idCommentaire` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `idMessage` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT pour la table `videos`
--
ALTER TABLE `videos`
  MODIFY `idVideo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commentaires`
--
ALTER TABLE `commentaires`
  ADD CONSTRAINT `fk_commentaires_users1` FOREIGN KEY (`users_idUser`) REFERENCES `users` (`idUser`),
  ADD CONSTRAINT `fk_commentaires_videos1` FOREIGN KEY (`videos_idVideo`) REFERENCES `videos` (`idVideo`);

--
-- Contraintes pour la table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `fk_likes_users1` FOREIGN KEY (`users_idUser`) REFERENCES `users` (`idUser`),
  ADD CONSTRAINT `fk_likes_videos1` FOREIGN KEY (`videos_idVideo`) REFERENCES `videos` (`idVideo`);

--
-- Contraintes pour la table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `fk_messages_users1` FOREIGN KEY (`idUserFrom`) REFERENCES `users` (`idUser`),
  ADD CONSTRAINT `fk_messages_users2` FOREIGN KEY (`idUserTo`) REFERENCES `users` (`idUser`);

--
-- Contraintes pour la table `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `fk_videos_users` FOREIGN KEY (`idUser`) REFERENCES `users` (`idUser`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
