//Imports
const express = require("express");
const cors = require("cors");
const fileController = require("../controllers/fileController");
const multerMiddelware=require("../middelware/multer")
const fileRouter = express.Router();
fileRouter.use(cors({ credentials: true, origin: "http://localhost:4200" }));
exports.router = (function() { 
fileRouter.route("/user/photo").post(multerMiddelware,fileController.createThing)
  return fileRouter;
})();
