//Imports
const publicationService=  require("./publicationService")
//Routes
module.exports = {
  store:function(req,res){
    let titreVideo = req.body.titreVideo;
    let nomVideo = req.body.nomVideo;
    let idUser = req.body.idUser;
    let extension = req.body.extension;

    if (
      titreVideo == null ||
      nomVideo == null ||
      idUser == null ||
      extension == null
    ) {
      return res.json({ success: false, msg: "Champs obligatoires" });
    }
    else
    {
    let video={
        titreVideo:titreVideo,
        nomVideo:nomVideo,
        idUser:idUser,
        extension:extension
    }
  publicationService.store(video).then(result =>
      res.status(result.status).send(result.payload));}
},
getVideosByUser: (req,res) =>{  
  idUser=req.param("id");
  publicationService.getVideosByUser(idUser).then(result =>
      res.status(result.status).send(result.payload));
},
delete:(req,res)=>{
  idVideo=req.param("id");
  console.log("id",idVideo)
  publicationService.delete(idVideo).then(result =>
      res.status(result.status).send(result.payload));
},
all:(req,res)=>{
  publicationService.all(res).then(result =>
      res.status(result.status).send(result.payload));
}
};
