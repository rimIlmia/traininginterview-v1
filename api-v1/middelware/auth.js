const jwt = require('jsonwebtoken');
const dotenv = require("dotenv");
dotenv.config();
module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.verify(token,process.env.JWT_SIGN_SECRET);
    const userId = decodedToken.idUser;
    if (decodedToken!=null) { 
         next();
    } else {
        throw 'Invalid user ID';
    }
  } catch {
    res.status(401).json({
      errors: 'Not authorized'
    });
  }
};