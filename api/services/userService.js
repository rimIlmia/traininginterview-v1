//Imports
const jwt = require("jsonwebtoken");
const jwtUtils = require("../utils/jwt.utils");
const connexion = require("../config/database");
const userQueries=require("../queries/userQueries")
const bcrypt = require("bcryptjs");
// Constants
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX = /^(?=.*\d).{8,12}$/;
//Routes
module.exports = {
  /*****************************Profil************************************* */
  profil: async function(req, res) {
    let headerAuth = req.headers["authorization"];
    let idUser = jwtUtils.getUserId(headerAuth);

    if (idUser < 0) {
      return {status:400,payload:{ success: false, msg: "l'utilisateur n'existe pas" }}
    }
     
         let user= await userQueries.getUserById(idUser);
          if(!user){
          return{status:400,payload: { success: false, msg: "user n'existe pas" }}
        } else {
            let { password, ...userWithoutPassword } = user;


          return ({status:200,payload:{ success: true, user: userWithoutPassword }})}
          
  },

  info: async function(id) {
  
  let user= await userQueries.getUserById(id);
          if(!user){
          return ({status:400,payload:{ success: false, msg: "user n'existe pas" }});
        } else {
            let { password, ...userWithoutPassword } = user;


          return({status:200,payload:{ success: true, user: userWithoutPassword }});}
          
  },
  /**********************update info user********** */
  update: async function(data) {


   
  let user= await userQueries.getUserById(data.idUser);
          if(!user){
            
          return ({status:400,payload:{ success: false, msg: "user n'existe pas" }});
        } else {
            let { password, ...userWithoutPassword } = user;
            console.log("user",userWithoutPassword);
            console.log("photo",userWithoutPassword.photoUrl)
         let newUrlPhoto=null
          if(userWithoutPassword.photoUrl!==data.photoUrl&& data.photoUrl!==null)
          {
          
            newUrlPhoto=data.urlPhoto
          }
          else
          {
            newUrlPhoto=user.urlPhoto

          }
          data.photoUrl=newUrlPhoto
          console.log("--------------------------")
          console.log(data)
       
   return userQueries.update(data).then(response => ({
				status: 200,
				payload: { success: true, message:"Mise à jour a été pris en compte" }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			}));
  }},

  /**Get user video */
  getUserByVideo: function(idVideo, res) {
    return userQueries.getUserByVideo(idVideo).then(response => ({
				status: 200,
				payload: { success: true, data: response }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			}));
  }
  
};
