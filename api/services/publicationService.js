//Imports
const publicationQueries=require("../queries/publicationQueries")
const commentQueries=require("../queries/commentQueries")

//Routes
module.exports = {
  /**store*/
  store: (video) =>{
  
  return publicationQueries.create(video).then(response => ({
				status: 200,
				payload: { success: true, msg:"Enregistré avec success" }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			})); 
  },
  getVideosByUser:(idUser, res)=>{
     return publicationQueries.getVideosByUser(idUser).then(response => ({
				status: 200,
				payload: { success: true, data: response }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			}));
  },
  delete:async(idVideo,res)=>{

    const deletedComment=await commentQueries.deleteByVideo(idVideo);
    if(deletedComment==true)
    {
       return publicationQueries.delete(idVideo,res).then(response => ({
				status: 200,
				payload: { success: true, msg:"Supprimé avec success" }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			})); 
    }
  },
  all:(res)=>{
     return publicationQueries.all(res).then(response => ({
				status: 200,
				payload: { success: true, data: response }
			}))
			.catch(err => ({
				status: 400,
				payload: { success: false, msg: err }
			}));
  }
};
