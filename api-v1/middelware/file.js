module.exports =(req, res, next) => {
    console.log("test")
    const acceptedTypeFile=["png","jpeg","jpg"];
    if(!req.file)
    {
        return res.json({success:false,message:"file required"})
    }
    const extension=req.file.mimetype.split('/').pop()
    if(!acceptedTypeFile.includes(extension))
    {
               return res.json({success:false,message:"file type not accepted"})
    }
    next()
  }


  
  const multer = require('multer');
const pify = require('pify');


const MIME_TYPES = {
  'image/jpg': 'jpg',
  'image/jpeg': 'jpg',
  'image/png': 'png',
  "video/mp4":'mp4',

};
const storage = multer.diskStorage({
  destination: (req, file, callback) => {
      const typeFile = file.mimetype.split('/');
      if(typeFile[0]==='image')
    callback(null, 'public/images');
    else if(typeFile[0]=='video')
    callback(null,'public/videos')
    // else {callback(null,{ err: 'File type not supported' })
  // }

  },
  
  filename: (req, file, callback) => {
    const name = file.originalname.split(' ').join('_');
    const extension = MIME_TYPES[file.mimetype];
    callback(null, name + Date.now() + '.' + extension);
  }
});

/* defined filter */
const fileFilter = (res, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
        cb(new Error("File format should be PNG,JPG,JPEG"), false); // if validation failed then generate error
  }
};

const upload = pify( multer({fileFilter:fileFilter,storage: storage}).single('myFile'))

module.exports =async(req,res,next)=>{
   try {
   const test=await upload(req, res)
   console.log("test", test)
    next()
  } catch(err) {
    return   res.status(400).json({
        message:multer.MulterError
      });
  }

 }



  
  
 

