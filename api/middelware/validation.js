const { body} = require('express-validator')

exports.validate=(method)=>{
  switch (method) {
    case 'register': {
     return [ 
        body('firstName', 'Prénom obligatoire').not().isEmpty(),
        body('lastName', 'Nom obligatoire').not().isEmpty(),
        body('email', 'Email obligatoire').not().isEmpty(),
        body('password','Mot de passe obligatoire').not().isEmpty(),
        body('email', 'Email invalide').isEmail(),
        body('firstName', 'Prénom obligatoire').not().isEmpty(),
        body('lastName', 'Nom obligatoire').not().isEmpty(),
        body("password", "Mot de passe doit contenir un melange de 8 lettres et chiffres").matches(/^(?=.*\d).{8,}$/, "i")
       ]   
    }
    case 'login':{
      return [
        body('email', 'Email obligatoire').not().isEmpty(),
        body('password','Mot de passe obligatoire').not().isEmpty(),
        body('email', 'Email invalide').isEmail(),

       ]
    }
  }

}