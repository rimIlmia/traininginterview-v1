const multer = require('multer');

const MIME_TYPES = {
  'image/jpg': 'jpg',
  'image/jpeg': 'jpg',
  'image/png': 'png',
  "video/mp4":'mp4',

};
const storage = multer.diskStorage({
  destination: (req, file, callback) => {
      const typeFile = file.mimetype.split('/');
      if(typeFile[0]==='image')
    callback(null, 'public/images');
    else if(typeFile[0]=='video')
    callback(null,'public/videos')
    else {callback(null,{ err: 'File type not supported' })
  }

  },
  
  filename: (req, file, callback) => {
    console.log("ici")
    const name = file.originalname.split(' ').join('_');
    const extension = MIME_TYPES[file.mimetype];
    callback(null, name + Date.now() + '.' + extension);
  }
});

module.exports = multer({storage: storage}).single('myFile');