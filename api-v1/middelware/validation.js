const { body,check} = require('express-validator')


exports.validate=(method)=>{
  switch (method) {
    case 'register': {
     return [ 
        body('firstName', 'Prénom obligatoire').not().isEmpty(),
        body('lastName', 'Nom obligatoire').not().isEmpty(),
        body('email', 'Email obligatoire').not().isEmpty(),
        body('password','Mot de passe obligatoire').not().isEmpty(),
        body('email', 'Email invalide').isEmail(),
        body('firstName', 'Prénom obligatoire').not().isEmpty(),
        body('lastName', 'Nom obligatoire').not().isEmpty(),
        body("password", "Mot de passe doit contenir un melange de 8 lettres et chiffres").matches(/^(?=.*\d).{8,}$/, "i")
       ]   
    }
    case 'login':{
      return [
        body('email', 'Email obligatoire').not().isEmpty(),
        body('password','Mot de passe obligatoire').not().isEmpty(),
        body('email', 'Email invalide').isEmail(),

       ]
      
    }
     case 'fileUpload':{
       return[
         check('myFile')
.custom((value, {req}) => {
        if(req.files.mimetype === 'image/png'){
            return 'png'; // return "non-falsy" value to indicate valid data"
        }else{
            return false; // return "falsy" value to indicate invalid data
        }
    })
.withMessage('Please only submit png documents.') // custom error message that will be send back if the file in not a pdf. 
  ]

       }
  }

}