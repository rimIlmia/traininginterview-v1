//Imports
const express = require("express");
const cors = require("cors");
const userController = require("./userController");
const fileController=require("../file/fileController")
const auth=require("../../middelware/auth")
const multerMiddelware=require("../../middelware/multer")

const path = require("path");
const userRouter = express.Router();
userRouter.use(cors({ credentials: true, origin: "http://localhost:4200" }));

exports.router = (function() { 
//routes
 userRouter.route("/me").get(auth,userController.profil);
 userRouter.route("/:id").get(auth,userController.getUserById);
 userRouter.route("/photo/:id").get(auth,userController.getPhoto);
 userRouter.route("/:id").put(auth,userController.update)
 userRouter.route("/video/:id").get(auth,userController.getUserByVideo);
 userRouter.route("/photo").post(multerMiddelware.multImage,fileController.upload);
 userRouter.route("/video").post(multerMiddelware.multVideo,fileController.upload);
  return userRouter;
})();