//Imports
const express = require("express");
const cors = require("cors");
const userController = require("../controllers/userController");
const auth=require("../middelware/auth")
const path = require("path");
const userRouter = express.Router();
userRouter.use(cors({ credentials: true, origin: "http://localhost:4200" }));

exports.router = (function() { 
//routes
 userRouter.route("/me").get(auth,userController.profil);
 userRouter.route("/:id").get(auth,userController.getUserById);
 userRouter.route("/photo/:fileid").get(auth,userController.getPhoto);
 userRouter.route("/:id").put(auth,userController.update)
 userRouter.route("/video/:idVideo").get(auth,userController.getUserByVideo);
  return userRouter;
})();